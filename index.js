const platform = process.platform;
const Path = require('path')
const {SRS_FILE_NAME} = require('./src/Constant')
const CmdUtils = require("./src/utils/CmdUtils");

let BASE_FILE = __dirname
let SRS = "srs"

/**
 * 获取srs路径
 */
function getSRSPath(){
    let fileName = getSRSFile();
    let cmdpath = Path.join(BASE_FILE,SRS,fileName);
    return cmdpath;
}

/**
 * 获取srs文件名称
 */
function getSRSFile(){
    let fileName;
    if("darwin"===platform){
        fileName = SRS_FILE_NAME.srs_darwin_arm64
    }else if("linux"===platform){
        fileName = SRS_FILE_NAME.srs_linux_x64
    }else{
        fileName = SRS_FILE_NAME.srs_win32_x64
    }
    return fileName;
}

/**
 *  启动
 *  @param cmdpath 启动地址 默认为包路径
 *  @param confpath 配置文件地址 默认为../conf
 */
function startSRS(cmdpath,confpath,calback){
    let cmd;
    let cmdstr;
    let conf = Path.join('..','srs.conf');//默认为包配置路径
    if(confpath){
        conf = confpath;
    }
    let path = getSRSPath();//默认为包路径
    if(cmdpath){
        path = cmdpath;
    }
    if("darwin"===platform){
        cmd = Path.join('objs','srs')
    }else if("linux"===platform){
        cmd = Path.join('objs','srs')
    }else{
        cmd = Path.join('objs','srs.exe')
    }
    cmdstr = ['-c',conf];

    CmdUtils.runExec(cmd,cmdstr,path,calback)
}

/**
 * 关闭
 */
function killSRS(calback){
    let cmd;
    let cmdstr;
    if("darwin"===platform){
        cmd = 'killall'
        cmdstr = ['srs'];
    }else if("linux"===platform){
        cmd = 'killall'
        cmdstr = ['srs'];
    }else{
        cmd = 'taskkill'
        cmdstr = ['/f','/t','/im','srs.exe'];
    }
    CmdUtils.runExec(cmd,cmdstr,null,calback)
}

/**
 * 启动websocket-hlv
 */
function startChangeWs(cmdpath,calback){
    let cmd;
    let cmdstr;
    let path = getSRSPath();//默认为包路径
    if(cmdpath){
        path = cmdpath;
    }
    if("darwin"===platform){
        cmd = Path.join('objs','goapp')
    }else if("linux"===platform){
        cmd = Path.join('objs','goapp')
    }else{
        cmd = Path.join('objs','goapp.exe')
    }
    cmdstr = [];

    CmdUtils.runExec(cmd,cmdstr,path,calback)
}

/**
 * 关闭websocket-hlv
 */
function killChangeWs(calback){
    let cmd;
    let cmdstr;
    if("darwin"===platform){
        cmd = 'killall'
        cmdstr = ['goapp'];
    }else if("linux"===platform){
        cmd = 'killall'
        cmdstr = ['goapp'];
    }else{
        cmd = 'taskkill'
        cmdstr = ['/f','/t','/im','goapp.exe'];
    }
    CmdUtils.runExec(cmd,cmdstr,null,calback)
}

module.exports = {
    BASE_FILE,
    SRS,
    getSRSPath,
    getSRSFile,
    startSRS,
    killSRS,
    startChangeWs,
    killChangeWs
}