const test = require('tape');
const {getSRSPath,startSRS,startChangeWs} = require('../index');

test('test', function (t) {
    console.info(getSRSPath())
    startSRS()
    let calback = {
        sendOutData: function (data) {
            console.log("[CmdUtils] stdout: "+data)
        }
    }
    startChangeWs(null,calback)
    t.end();
});