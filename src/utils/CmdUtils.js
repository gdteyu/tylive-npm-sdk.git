const cp = require('child_process')
const Log = require('loglevel');
const platform = process.platform;

function CmdUtils(){}

/**
 * 执行命令
 */
function runExec(cmd,cmdstr,cmdpath,calback) {
    let options = {}
    if(cmdpath){
        options.cwd = cmdpath
    }

    let workerprocess = cp.spawn(cmd,cmdstr,options);

    // 打印正常的后台可执行程序输出
    workerprocess.stdout.on('data', function (data) {
        Log.info('[CmdUtils] stdout: ' + data)
        if(calback&&typeof calback.sendOutData === 'function'){
            calback.sendOutData(data);
        }
    })

    // 打印错误的后台可执行程序输出
    workerprocess.stderr.on('data', function (data) {
        Log.info('[CmdUtils] stderr: ' + data)
        if(calback&&typeof calback.sendErrData === 'function'){
            calback.sendErrData(data);
        }
    })

    // 退出之后的输出
    workerprocess.on('close', function (code) {
        Log.info('[CmdUtils] out code：' + code)
        if(calback&&typeof calback.close === 'function'){
            calback.close(code);
        }
    })
}

function killByPid(pid,signal){
    let cmd;
    let cmdstr;
    if("darwin"===platform){
        cmd = 'kill';
        cmdstr = ['-9',pid];
    }else if("linux"===platform){
        cmd = 'kill';
        cmdstr = ['-9',pid];
    }else{
        cmd = 'taskkill';
        cmdstr = ['/f','/pid',pid];
    }
    runExec(cmd,cmdstr);

    //当进程不存在会出错
    //process.kill(pid , signal||'SIGKILL')
}

if (typeof CmdUtils !== 'undefined') {
    CmdUtils.runExec = runExec;
    CmdUtils.killByPid = killByPid;
}

module.exports = CmdUtils