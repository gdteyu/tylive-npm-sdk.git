/**
 * srs文件夹名称
 * @type {{srs_darwin_arm64: string, srs_win32_x64: string, srs_linux_x64: string}}
 */
let SRS_FILE_NAME = {
    srs_darwin_arm64 : 'darwin-arm64',
    srs_linux_x64 : 'linux-x64',
    srs_win32_x64 : 'win32-x64',
}

module.exports = {
    SRS_FILE_NAME
}
